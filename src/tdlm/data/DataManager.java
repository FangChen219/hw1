package tdlm.data;

import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;

import tdlm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;


/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_OWNER = "";
    // NAME OF THE TODO LIST
    StringProperty name;
    
    // LIST OWNER
    StringProperty owner;
    
    // THESE ARE THE ITEMS IN THE TODO LIST
    ObservableList<ToDoItem> items;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        // Workspace workspace = (Workspace)app.getWorkspaceComponent();
        items=FXCollections.observableArrayList();
        name=new SimpleStringProperty(DEFAULT_NAME);
        owner=new SimpleStringProperty(DEFAULT_OWNER);
        //name.setValue("");
        //name.setValue(workspace.getName());
        
        //owner.setValue("");
        //owner.setValue(workspace.getOwner());
        
    }
    
    public ObservableList<ToDoItem> getItems() {
	return items;
    }
    
    public void setName(String getName){
        name.set(getName);
    }
    public void setOwner(String getOwner){
        name.set(getOwner);
    }
    
    public String getName() {
        
        return name.get();
        
    }
    
    public String getOwner() {
        return owner.get();
    }

    public void addItem(ToDoItem item) {
         
        items.add(item);
         //items.addAll(item);
    }



    /**
     * 
     */
    @Override
    public void reset() {
        name=new SimpleStringProperty("");
        owner=new SimpleStringProperty("");
        items.clear();
    }
}
