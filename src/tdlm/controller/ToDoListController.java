package tdlm.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.HPos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.DatePicker;
import javafx.scene.control.CheckBox;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.data.ToDoItem;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppPropertyType.APP_CSS;
import saf.ui.AppGUI;
import saf.ui.AppMessageDialogSingleton;
import tdlm.PropertyType;
import static saf.settings.AppPropertyType.APP_PATH_CSS;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController extends AppWorkspaceComponent{
    
    static final String CLASS_DIALOG_TITLE = "dialog_title";
    static final String CLASS_DIALOG_LABEL = "dialog_label";
    static final String CLASS_DIALOG_PANE = "dialog_pane";
    //static final String CLASS_DIALOG_TITLE = "heading_label";
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
     static final int LARGE_TEXT_FIELD_LENGTH = 20;
    AppTemplate app;
    AppMessageDialogSingleton messageDialog;
    AppGUI gui;
    Stage windowStage;
    Pane newWindow;
    GridPane newWG;
    Label dialogTitle;
    Label CategoryLabel;
    Label DescribeLabel;
    Label StartDateLabel;
    Label EndDateLabel;
    Label CompletedLabel;
    TextField CategoryTextField;
    TextField DescribeTextField;
    DatePicker StartDatePicker;
    DatePicker EndDatePicker;
    CheckBox Completed;
    Button AddBtn;
    Button CancelBtn;
    ToDoItem item;
    DataManager data;
    HBox CategoryBox;
    HBox DescribeBox;
    HBox StartDateBox;
    HBox EndDateBox;
    HBox CompleteBox;
    HBox ButtonBox;
    VBox newDialog;
    String name;
    String owner;
    Scene newscene;
    boolean ADD=true;
    String stylesheet;
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
        gui=app.getGUI();
        //app.initStylesheet();
        
        
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS
	
	//workspace.reloadWorkspace();
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        workspace.updateText();
        //workspace.newDialog();
        //workspace.addList();
        newDialog();
        initStyle();
       /* windowStage= new Stage();
        windowStage.setTitle("Add list");
        
        newWindow= new Pane();
        newWG= new GridPane();
        
        Scene newScene=new Scene(newWindow,400,250);
        newWG.setHgap(6);
        newWG.setVgap(6);
        CategoryLabel= new Label();
        CategoryLabel.setText("Category: ");
        newWG.add(CategoryLabel, 0, 0);
        CategoryTextField=new TextField();
        newWG.add(CategoryTextField, 1, 0);
        DescribeLabel= new Label();
        DescribeLabel.setText("Describe: ");
        newWG.add(DescribeLabel, 0, 1);
        DescribeTextField=new TextField();
        newWG.add(DescribeTextField, 1, 1); 
        StartDateLabel= new Label();
        StartDateLabel.setText("Start Date ");
        newWG.add(StartDateLabel, 0, 2);
        StartDatePicker=new DatePicker();
        newWG.add(StartDatePicker, 1, 2);
        EndDateLabel= new Label();
        EndDateLabel.setText("End Date: ");
        newWG.add(EndDateLabel, 0, 3);
        EndDatePicker=new DatePicker();
        newWG.add(EndDatePicker, 1, 3);
        CompletedLabel=new Label();
        CompletedLabel.setText("Completed ");
        newWG.add(CompletedLabel, 0, 4);
        Completed=new CheckBox();
        newWG.add(Completed, 1, 4);
        AddBtn=new Button("Add");
        newWG.add(AddBtn, 1, 5);
        GridPane.setHalignment(AddBtn, HPos.CENTER);
         newWindow.getChildren().add(newWG);
        windowStage.setScene(newScene);*/
        
        AddBtn.setOnAction(e -> {
            AddList();
        });
        CancelBtn.setOnAction(e ->{
            windowStage.close();
        });
        
        windowStage.show();
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //workspace.reloadWorkspace();
    }
    
    public void processRemoveItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        workspace.updateText();
        data=(DataManager)app.getDataComponent();
        item=workspace.selectedItem();
        data.getItems().removeAll(item);
    }
    
    public void processMoveUpItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        data=(DataManager)app.getDataComponent();
        item=workspace.selectedItem();
        
        int index;
        index=workspace.getIndex();
        ToDoItem uperItem;
        uperItem=data.getItems().get(index-1);
        data.getItems().set(index-1, item);
        data.getItems().set(index,uperItem);
        
        
    }
    
    public void processMoveDownItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        data=(DataManager)app.getDataComponent();
        item=workspace.selectedItem();
        
        int index;
        index=workspace.getIndex();
        ToDoItem uperItem;
        uperItem=data.getItems().get(index+1);
        data.getItems().set(index+1, item);
        data.getItems().set(index,uperItem);
    }
    
    public void processEditItem() {
        ADD=false;
        newDialog();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        
        
        EditDialog();
       AddBtn.setOnAction(e -> {
            EditList();
        });
          CancelBtn.setOnAction(e ->{
            windowStage.close();
        });
         windowStage.show();
        
    }
    public void newDialog(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        windowStage= new Stage();
        //messageDialog.init(windowStage);
        dialogTitle=new Label();
        
        if(ADD)
            dialogTitle.setText(props.getProperty(PropertyType.DIALOG_TITLE_ADD));
        else
            dialogTitle.setText(props.getProperty(PropertyType.DIALOG_TITLE_EDIT));
        dialogTitle.getStyleClass().add(CLASS_DIALOG_TITLE);
       //dialogTitle.setStyle(CLASS_DIALOG_TITLE);
        CategoryBox=new HBox();
        CategoryLabel= new Label();
        CategoryLabel.setText(props.getProperty(PropertyType.CATEGORY_LABEL));
        CategoryTextField=new TextField();
        CategoryBox.getChildren().addAll( CategoryLabel, CategoryTextField);
        DescribeBox=new HBox();
        DescribeLabel= new Label();
        DescribeLabel.setText(props.getProperty(PropertyType.DESCRIPTION_LABEL));
        DescribeTextField=new TextField();
        DescribeBox.getChildren().addAll(DescribeLabel,DescribeTextField);
        StartDateBox=new HBox();
        StartDateLabel= new Label();
        StartDateLabel.setText(props.getProperty(PropertyType.START_DATE_LABEL));
        StartDatePicker=new DatePicker();
        StartDateBox.getChildren().addAll(StartDateLabel,StartDatePicker);
        EndDateBox=new HBox();
        EndDateLabel= new Label();
        EndDateLabel.setText(props.getProperty(PropertyType.END_DATE_LABEL));
        EndDatePicker=new DatePicker();
        EndDateBox.getChildren().addAll(EndDateLabel,EndDatePicker);
        CompleteBox=new HBox();
        CompletedLabel=new Label();
        CompletedLabel.setText(props.getProperty(PropertyType.COMPLETED_LABEL));
        Completed=new CheckBox();
        CompleteBox.getChildren().addAll(CompletedLabel,Completed);
        AddBtn = new Button();
        AddBtn.setText(props.getProperty(PropertyType.ADD_BTN));
        CancelBtn = new Button();
        CancelBtn.setText(props.getProperty(PropertyType.CANCEL_BTN));
        ButtonBox=new HBox();
        ButtonBox.getChildren().addAll(AddBtn,CancelBtn);
        newDialog=new VBox();
        
        newDialog.getChildren().addAll( dialogTitle,CategoryBox,DescribeBox,StartDateBox,EndDateBox,CompleteBox,ButtonBox);
        //newWindow=new Pane();
         initStyle();
         
        //newWindow.getChildren().addAll(newDialog);
//newWG= new GridPane();
        //newWG.add(newDialog, 0, 0);
        //GridPane.setHalignment(newDialog, HPos.CENTER);
        newscene=new Scene(newDialog,500,700);
        
            //  newscene.getStylesheets().addAll(workspace.getStylesheets());
            //Stylesheet();
        
        //newScene.getStylesheets().add(stylesheet);
        windowStage.setScene(newscene);
        
        
//newWindow.getChildren().add(newDialog);
        
        
        //newWG= new GridPane();
       
        
        
    }
    public void AddList(){
       
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        data=(DataManager)app.getDataComponent();
        String GetCategory;
        String Describe;
        LocalDate StartDate;
        LocalDate EndDate;
        Boolean Complete;
        GetCategory=CategoryTextField.getText();
        Describe=DescribeTextField.getText();
        //StartDate=StartDatePicker.getEditor().getText();
        StartDate=StartDatePicker.getValue();
        EndDate=EndDatePicker.getValue();
        Complete=Completed.isSelected();
        item=new ToDoItem(GetCategory,Describe,StartDate,EndDate,Complete);
        data.addItem(item);
        //workspace.reloadWorkspace();
        windowStage.close();
        /*System.out.println( GetCategory);
        System.out.println( Describe);
        System.out.println( StartDate);
        System.out.println( EndDate);
        System.out.println( Complete);*/
       
        
         //Platform.runLater(()->{
            
             //data.addItem(item);  
        //});
        //test=false;
       /* workspace.reloadWorkspace();
        windowStage.close();*/
       
        
      //  GetCategory = newWG.get               //CategoryTextFeild().getText();
        
        
        
        
        
    }
    public void EditDialog(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        data=(DataManager)app.getDataComponent();
        item=workspace.selectedItem();
        //CategoryTextField=new TextField();
        if(item.getCategory()!=null)
            CategoryTextField.setText(item.getCategory());
        //escribeTextField=new TextField();
        if(item.getDescription()!=null)
            DescribeTextField.setText(item.getDescription());
        if(item.getStartDate()!= null)
            StartDatePicker.setValue(item.getStartDate());
        if(item.getEndDate()!=null)
            EndDatePicker.setValue(item.getEndDate());
        
        Completed.setSelected(item.getCompleted());
    }
    public void EditList(){
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
        data=(DataManager)app.getDataComponent();
        String GetCategory;
        String Describe;
        LocalDate StartDate;
        LocalDate EndDate;
        Boolean Complete;
        GetCategory=CategoryTextField.getText();
        Describe=DescribeTextField.getText();
        //StartDate=StartDatePicker.getEditor().getText();
        StartDate=StartDatePicker.getValue();
        EndDate=EndDatePicker.getValue();
        Complete=Completed.isSelected();
        item.setCategory(GetCategory);
        item.setDescription(Describe);
        item.setStartDate(StartDate);
        item.setEndDate(EndDate);
        item.setCompleted(Complete);
         windowStage.close();
    }
   @Override
    public void initStyle(){
        newDialog.getStyleClass().add(CLASS_DIALOG_PANE);
        
        dialogTitle.getStyleClass().add(CLASS_DIALOG_TITLE);
        CategoryLabel.getStyleClass().add(CLASS_DIALOG_LABEL);
        DescribeLabel.getStyleClass().add(CLASS_DIALOG_LABEL);
        StartDateLabel.getStyleClass().add(CLASS_DIALOG_LABEL);
        EndDateLabel.getStyleClass().add(CLASS_DIALOG_LABEL);
        CompletedLabel.getStyleClass().add(CLASS_DIALOG_LABEL);
        CategoryBox.getStyleClass().add(CLASS_DIALOG_PANE);
        DescribeBox.getStyleClass().add(CLASS_DIALOG_PANE);
        StartDateBox.getStyleClass().add(CLASS_DIALOG_PANE);
        EndDateBox.getStyleClass().add(CLASS_DIALOG_PANE);
        CompleteBox.getStyleClass().add(CLASS_DIALOG_PANE);
        ButtonBox.getStyleClass().add(CLASS_DIALOG_PANE);
        
        //dialogTitle.setStyle(CLASS_DIALOG_TITLE);
        newDialog.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 10;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        CategoryLabel.setStyle("-fx-font-size: 14pt;\n" +
"    -fx-font-weight: bold;");
        DescribeLabel.setStyle("-fx-font-size: 14pt;\n" +
"    -fx-font-weight: bold;");
        StartDateLabel.setStyle("-fx-font-size: 14pt;\n" +
"    -fx-font-weight: bold;");
        EndDateLabel.setStyle("-fx-font-size: 14pt;\n" +
"    -fx-font-weight: bold;");
        CompletedLabel.setStyle("-fx-font-size: 14pt;\n" +
"    -fx-font-weight: bold;");
        CategoryBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 50;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        DescribeBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 30;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        StartDateBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 25;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        EndDateBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 30;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        CompleteBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 10;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        ButtonBox.setStyle("-fx-background-color: #ffffff;\n" +
"    -fx-background-radius: 5.0;\n" +
"    -fx-padding: 15;\n" +
"    -fx-spacing: 50;\n" +
"    -fx-border-width: 2px;\n" +
"    -fx-border-color: #7777dd;");
        
        
    }

    @Override
    public void reloadWorkspace() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
  /* public void Stylesheet() {
       PropertiesManager props = PropertiesManager.getPropertiesManager();
	stylesheet = props.getProperty(APP_PATH_CSS);
	stylesheet += props.getProperty(APP_CSS);
        File f=new File(tdlm_style.css);
	URL stylesheetURL = getClass().getResource("css/tdlm_style.css");
        URL stylesheetURL;
       stylesheetURL=new URL(stylesheet);
	String stylesheetPath = stylesheetURL.toExternalForm();
        //newscene.getStylesheets().add(stylesheetPath);
   }*/
            
}
